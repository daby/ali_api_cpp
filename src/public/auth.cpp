#include "core.h"

void quicksortHeadersByKey(std::vector<std::string> &v, int left, int right)
{
	if(left < right){
		std::string key = v[left];
		int low = left;
		int high = right;
		while(low < high){
			while(low < high && strcmp(v[high].c_str(), key.c_str()) >= 0){
				high--;
			}
			v[low] = v[high];
			while(low < high && strcmp(v[low].c_str(), key.c_str()) < 0){
				low++;
			}
			v[high] = v[low];
		}
		v[low] = key;
		quicksortHeadersByKey(v,left,low-1);
		quicksortHeadersByKey(v,low+1,right);
	}
}

/************************************************************************/
/* url ����� ��php�н��                                               */
/************************************************************************/
int php_htoi(char *s)
{
	int value;
	int c;

	c = ((unsigned char *)s)[0];
	if (isupper(c))
		c = tolower(c);
	value = (c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10) * 16;

	c = ((unsigned char *)s)[1];
	if (isupper(c))
		c = tolower(c);
	value += c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10;

	return (value);
}

std::string urldecode(const std::string &str_source)
{
	char const *in_str = str_source.c_str();
	int in_str_len = strlen(in_str);
	int out_str_len = 0;
	std::string out_str;
	char *str;

#ifdef _WIN32
	str = _strdup(in_str);
#elif defined linux
	str = strdup(in_str);
#endif
	char *dest = str;
	char *data = str;

	while (in_str_len--) {
//		if (*data == '+') {
//			*dest = ' ';
//		}
//		else 
		if (*data == '%' && in_str_len >= 2 && isxdigit((int) *(data + 1)) 
			&& isxdigit((int) *(data + 2))) {
				*dest = (char) php_htoi(data + 1);
				data += 2;
				in_str_len -= 2;
		} else {
			*dest = *data;
		}
		data++;
		dest++;
	}
	*dest = '\0';
	out_str_len =  dest - str;
	out_str = str;
	free(str);
    return out_str;
}

std::string urlencode(const std::string &str_source)
{
	char const *in_str = str_source.c_str();
	int in_str_len = strlen(in_str);
	int out_str_len = 0;
	std::string out_str;
	register unsigned char c;
	unsigned char *to, *start;
	unsigned char const *from, *end;
	unsigned char hexchars[] = "0123456789ABCDEF";

	from = (unsigned char *)in_str;
	end = (unsigned char *)in_str + in_str_len;
	start = to = (unsigned char *) malloc(3*in_str_len+1);

	while (from < end) {
		c = *from++;

//		if (c == ' ') {
//			*to++ = '+';
//		} else
		if ((c < '0' && c != '-' && c != '.') ||
			(c < 'A' && c > '9') ||
			(c > 'Z' && c < 'a' && c != '_') ||
			(c > 'z')) { 
				to[0] = '%';
				to[1] = hexchars[c >> 4];
				to[2] = hexchars[c & 15];
				to += 3;
		} else {
			*to++ = c;
		}
	}
	*to = 0;

	out_str_len = to - start;
	out_str = (char *) start;
	free(start);
	return out_str;
}