#ifndef __WINSYS_H__
#define __WINSYS_H__
#pragma once

#ifdef _WIN32

#ifndef _WINSOCK2API_  
#include <WinSock2.h>  
#endif

#ifndef _WINDOWS_  
#include <Windows.h>
#endif

#include <stdio.h>
#include <Wincrypt.h>
#include <shlwapi.h>
#include <Mswsock.h>
#include <time.h>
#include <assert.h>
#include <crtdbg.h>
#include <process.h>
#include <tchar.h>
#include <stdint.h>
#include <direct.h>

#include <stdlib.h>
#include <crtdbg.h>

#pragma comment( lib, "ws2_32.lib" )
#pragma comment(lib, "shlwapi.lib")

typedef unsigned char u8;
typedef unsigned short u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef char s8;
typedef short s16;
typedef int32_t s32;
typedef int64_t s64;

#define THREAD_FUN unsigned int
#define ThreadID DWORD

#define CSLEEP(n) Sleep(n)
#define SafeSprintf sprintf_s

#endif //_WIN32

#endif