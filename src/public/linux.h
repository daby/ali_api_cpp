#ifndef __LINUX_H__
#define __LINUX_H__
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/time.h>
#include <libgen.h>
#include <assert.h>
#include <sys/wait.h>
#include <dlfcn.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef char s8;
typedef short s16;
typedef int32_t s32;
typedef int64_t s64;

#define THREAD_FUN void*
#define ThreadID pthread_t

#define CSLEEP(n) usleep(n)
#define SafeSprintf snprintf

#endif