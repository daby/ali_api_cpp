#ifndef __ADDTAGS_H__
#define __ADDTAGS_H__
#pragma once

class CProduct;

class DLL_PRODUCT_API CApi
{
public:
	CApi(CProduct *ptr);
	~CApi();
	
	void parseMeta(std::string file);

	void addQuerryParameter(std::string key, std::string value);
	void getResponseParameter(std::string key, std::string &value);
	std::string GetQuerryParameter();
	SResponseMap& GetResponse(){return m_responseMap;}

	//request
	std::string method;
	std::string protocol;
	std::string name;
	std::string product;
	std::string version;
	std::map<std::string, SRequest> m_mapRequestParameter;

	//response
	struct SResponseMap m_responseMap;

	CProduct *m_pEcs;
};


#endif