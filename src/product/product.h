#ifndef __PRODUCT_H__
#define __PRODUCT_H__
#pragma once

class CApi;

class DLL_PRODUCT_API CProduct
{
public:
	static CProduct* getInstance();

	bool Initialize(std::string product);
	bool Destory(std::string product);

	bool Request(std::string queryParam, std::string name);
	bool Response(SResponseMap &mapResult);

	void setDomain(std::string domain);
	void setBaseParameter(std::string AccessKeyId, std::string AccessKeySecret, std::string ResourceOwnerAccount, std::string retFormat = "XML",
							std::string method = "GET", std::string protocol = "HTTP");

	CApi *m_pApi;
	std::string m_queryUrl;
	std::string m_accessKeyId;
	std::string m_accessKeySecret;
	std::string m_resourceOwnerAccount;
	std::string m_retFormat;
	std::string m_method;
	std::string m_protocol;
	std::string m_actionName;
private:
	CProduct();
	~CProduct();
};


#endif