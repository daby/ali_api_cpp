#include "core.h"

int main()
{
	CProduct* p = CProduct::getInstance();
	//AddTags
#ifdef _WIN32
	std::string path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\AddTags.json";
#elif defined linux
	std::string path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/AddTags.json";
#endif
	p->m_pApi->parseMeta(path.c_str());
	p->setDomain("https://ecs.aliyuncs.com");
	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");

	p->m_pApi->addQuerryParameter("RegionId", "cn-shenzhen");
	p->m_pApi->addQuerryParameter("ResourceType", "snapshot");
	p->m_pApi->addQuerryParameter("ResourceId", "s-94c39s4rz");
	p->m_pApi->addQuerryParameter("Tag.1.Key", "test");
	p->m_pApi->addQuerryParameter("Tag.1.Value", "value1");
	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
	{
		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
		return 0;
	}
	p->Response(p->m_pApi->GetResponse());

	std::vector<struct SMember>::iterator it = p->m_pApi->GetResponse().members.begin();
	for (; it!=p->m_pApi->GetResponse().members.end(); it++)
	{
		printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
	}
	
	//DescribeTags
#ifdef _WIN32
	path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\DescribeTags.json";
#elif defined linux
	path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/DescribeTags.json";
#endif
	p->m_pApi->parseMeta(path.c_str());
	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");

	p->m_pApi->addQuerryParameter("RegionId", "cn-shenzhen");
	p->m_pApi->addQuerryParameter("ResourceType", "snapshot");
	p->m_pApi->addQuerryParameter("ResourceId", "s-94c39s4rz");
	p->m_pApi->addQuerryParameter("Tag.1.Key", "test");
	p->m_pApi->addQuerryParameter("Tag.1.Value", "value1");
	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
	{
		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
		return 0;
	}
	p->Response(p->m_pApi->GetResponse());

	it = p->m_pApi->GetResponse().members.begin();
	for (; it!=p->m_pApi->GetResponse().members.end(); it++)
	{
		printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
	}
	std::vector<struct SResponseMap*>::iterator it_arrays = p->m_pApi->GetResponse().vArraysNode.begin();
	for (; it_arrays!=p->m_pApi->GetResponse().vArraysNode.end(); it_arrays++)
	{
		it = (*it_arrays)->members.begin();
		for (; it!=(*it_arrays)->members.end(); it++)
		{
			printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
		}
	}

	//DescribeResourceByTags
#ifdef _WIN32
	path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\DescribeResourceByTags.json";
#elif defined linux
	path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/DescribeResourceByTags.json";
#endif
	p->m_pApi->parseMeta(path.c_str());
	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");

	p->m_pApi->addQuerryParameter("RegionId", "cn-shenzhen");
	p->m_pApi->addQuerryParameter("ResourceType", "snapshot");
	p->m_pApi->addQuerryParameter("ResourceId", "s-94c39s4rz");
	p->m_pApi->addQuerryParameter("Tag.1.Key", "test");
	p->m_pApi->addQuerryParameter("Tag.1.Value", "value1");
	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
	{
		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
		return 0;
	}
	p->Response(p->m_pApi->GetResponse());

	it = p->m_pApi->GetResponse().members.begin();
	for (; it!=p->m_pApi->GetResponse().members.end(); it++)
	{
		printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
	}
	it_arrays = p->m_pApi->GetResponse().vArraysNode.begin();
	for (; it_arrays!=p->m_pApi->GetResponse().vArraysNode.end(); it_arrays++)
	{
		it = (*it_arrays)->members.begin();
		for (; it!=(*it_arrays)->members.end(); it++)
		{
			printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
		}
	}

	//StartInstance
// #ifdef _WIN32
// 	path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\StartInstance.json";
// #elif defined linux
// 	path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/StartInstance.json";
// #endif
// 	p->m_pApi->parseMeta(path.c_str());
// 	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");
// 
// 	p->m_pApi->addQuerryParameter("InstanceId", "i-945ffsle8");
// 	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
// 	{
// 		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
// 		return 0;
// 	}
// 	p->Response(p->m_pApi->GetResponse());

	//StopInstance
// #ifdef _WIN32
// 	path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\StopInstance.json";
// #elif defined linux
// 	path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/StopInstance.json";
// #endif
// 	p->m_pApi->parseMeta(path.c_str());
// 	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");
// 
// 	p->m_pApi->addQuerryParameter("InstanceId", "i-945ffsle8");
// 	p->m_pApi->addQuerryParameter("ForceStop", "false");
// 	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
// 	{
// 		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
// 		return 0;
// 	}
// 	p->Response(p->m_pApi->GetResponse());

	//RemoveTags
#ifdef _WIN32
	path = "..\\..\\..\\openapi-meta\\trunk\\api-metadata\\aliyun-api-metadata-ecs\\2014-05-26\\Api\\RemoveTags.json";
#elif defined linux
	path = "../../../openapi-meta/trunk/api-metadata/aliyun-api-metadata-ecs/2014-05-26/Api/RemoveTags.json";
#endif
	p->m_pApi->parseMeta(path.c_str());
	p->setBaseParameter(ACCESSKEYID, ACCESSKEYSECRET, "xifandezhexue@163.com", "XML", "POST");

	p->m_pApi->addQuerryParameter("RegionId", "cn-shenzhen");
	p->m_pApi->addQuerryParameter("ResourceType", "snapshot");
	p->m_pApi->addQuerryParameter("ResourceId", "s-94c39s4rz");
	p->m_pApi->addQuerryParameter("Tag.1.Key", "test");
	p->m_pApi->addQuerryParameter("Tag.1.Value", "value1");
	if (!p->Request(p->m_pApi->GetQuerryParameter().c_str(), p->m_pApi->name))
	{
		printf("[%s]:%d\n", __FUNCTION__, __LINE__);
		return 0;
	}
	p->Response(p->m_pApi->GetResponse());

	it = p->m_pApi->GetResponse().members.begin();
	for (; it!=p->m_pApi->GetResponse().members.end(); it++)
	{
		printf("name:%s value:%s\n", it->tagName.c_str(), it->value.c_str());
	}
}